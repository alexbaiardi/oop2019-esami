package a01a.e2;

import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class GUI extends JFrame {
	
	private final Logic logic;
	private final Map<JButton,Pair<Integer, Integer>> buttons;
    
    public GUI(int size, int boat) { 
    	
    	logic=new LogicImpl(size, boat);
    	buttons=new HashMap<>();
    	this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(size*100, size*100);
        JPanel panel = new JPanel(new GridLayout(size,size));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();
            bt.setText(logic.hit(buttons.get(bt).getX(), buttons.get(bt).getY())?"X":"O");
            bt.setEnabled(false);
            if(logic.isOver())
            {
            	System.out.println(logic.getResult()?"Hai vinto":"Hai perso");
            	System.exit(0);
            }
        };
        for (int i=0;i<size;i++){
        	for(int j=0;j<size;j++) {
	            final JButton jb = new JButton("");
	            jb.addActionListener(al);
	            panel.add(jb);
	            buttons.put(jb, new Pair<>(i, j));
        	}
        } 
    	this.setVisible(true);
    }
    
}
