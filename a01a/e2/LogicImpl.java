package a01a.e2;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class LogicImpl implements Logic {

	private static final int MAX_ATTEMPTS=5;
	private final Set<Pair<Integer,Integer>> boat;
	private int attempts;
	private final int size;
	
	
	
	public LogicImpl(int size,int boatLenght) {
		this.attempts=MAX_ATTEMPTS;
		this.size = size;
		this.boat=new HashSet<>();
		inizializeBoat(boatLenght);
	}

	private void inizializeBoat(int boatLenght) {
		Random r=new Random();
		Pair<Integer,Integer> initPos=new Pair<Integer, Integer>(r.nextInt(size), r.nextInt(size-boatLenght));
		System.out.println(initPos);
		for(int i=0;i<boatLenght;i++){
			boat.add(new Pair<>(initPos.getX(),initPos.getY()+i));
		}		
	}

	@Override
	public boolean hit(int x, int y) {
		attempts--;
		return boat.remove(new Pair<>(x, y));
	}

	@Override
	public boolean isOver() {
		return attempts==0||boat.isEmpty();
	}

	@Override
	public boolean getResult() {
		return boat.isEmpty();
	}

}
