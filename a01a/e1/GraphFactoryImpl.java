package a01a.e1;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GraphFactoryImpl implements GraphFactory {

	private <X> Graph<X> createGraph(Set<X> n,Set<Pair<X, X>> e){
		return new Graph<X>() {
					
					Set<X> nodes=n;
					Set<Pair<X, X>> edges=e;
		
					@Override
					public Set<X> getNodes() {
						return nodes;
					}
		
					@Override
					public boolean edgePresent(X start, X end) {
						return edges.contains(new Pair<>(start, end));
					}
					@Override
					public int getEdgesCount() {
						return edges.size();
					}
		
					@Override
					public Stream<Pair<X, X>> getEdgesStream() {
						return edges.stream();
					}
					
				};
	}
	
	@Override
	public <X> Graph<X> createDirectedChain(List<X> nodes) {
		Set<Pair<X, X>> edges=new HashSet<>();
		for(int i=0;i<nodes.size()-1;i++) {
			edges.add(new Pair<X, X>(nodes.get(i),nodes.get(i+1)));			
		}
		return createGraph(nodes.stream().collect(Collectors.toSet()), edges);
	}

	@Override
	public <X> Graph<X> createBidirectionalChain(List<X> nodes) {
		Set<Pair<X, X>> edges=new HashSet<>();
		for(int i=0;i<nodes.size()-1;i++) {
			edges.add(new Pair<X, X>(nodes.get(i),nodes.get(i+1)));
			edges.add(new Pair<X, X>(nodes.get(i+1),nodes.get(i)));
		}
		return createGraph(nodes.stream().collect(Collectors.toSet()), edges);
	}

	@Override
	public <X> Graph<X> createDirectedCircle(List<X> nodes) {
		Set<Pair<X, X>> edges=new HashSet<>();
		for(int i=0;i<nodes.size();i++) {
			edges.add(new Pair<X, X>(nodes.get(i),nodes.get((i+1)%nodes.size())));
		}
		return createGraph(nodes.stream().collect(Collectors.toSet()), edges);
	}

	@Override
	public <X> Graph<X> createBidirectionalCircle(List<X> nodes) {
		Set<Pair<X, X>> edges=new HashSet<>();
		for(int i=0;i<nodes.size();i++) {
			edges.add(new Pair<X, X>(nodes.get(i),nodes.get((i+1)%nodes.size())));
			edges.add(new Pair<X, X>(nodes.get((i+1)%nodes.size()),nodes.get(i)));
		}
		return createGraph(nodes.stream().collect(Collectors.toSet()), edges);
	}

	@Override
	public <X> Graph<X> createDirectedStar(X center, Set<X> nodes) {
		
		Set<Pair<X, X>> edges=nodes.stream().map(n->new Pair<>(center, n)).collect(Collectors.toSet());
		Set<X> n=new HashSet(nodes);
		n.add(center);
		return createGraph(n, edges);
	}

	@Override
	public <X> Graph<X> createBidirectionalStar(X center, Set<X> nodes) {
		Set<Pair<X, X>> edges=nodes.stream().flatMap(n->Stream.of(new Pair<>(center, n),new Pair<>(n,center))).collect(Collectors.toSet());
		Set<X> n=new HashSet(nodes);
		n.add(center);
		return createGraph(n, edges);
	}

	@Override
	public <X> Graph<X> createFull(Set<X> nodes) {
		Set<Pair<X, X>> edges=nodes.stream().flatMap(n->nodes.stream().map(node->new Pair(n, node))).filter(p->!p.getX().equals(p.getY())).collect(Collectors.toSet());
		return createGraph(nodes.stream().collect(Collectors.toSet()), edges);
	}

	@Override
	public <X> Graph<X> combine(Graph<X> g1, Graph<X> g2) {
		
		return null;
	}

}
