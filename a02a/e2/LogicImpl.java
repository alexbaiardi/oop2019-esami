package a02a.e2;

import java.util.HashSet;
import java.util.Set;


public class LogicImpl implements Logic {
    
    private final int size;
    private int actualSize;
    private Set<Pair<Integer, Integer>> marked;
    private boolean versus=false;
    

    public LogicImpl(int size) {
        this.size = size;
        actualSize=size;
        marked=new HashSet<>();
        mark(size,0);
    }

    @Override
    public void update() {
        int offset;
        
        actualSize = versus?actualSize+2:actualSize-2;
        offset = (size - actualSize) / 2;
        marked.clear();
        mark(actualSize, offset);
        if(actualSize==1||actualSize==size) {
            versus=!versus;
        }
    }

    @Override
    public boolean isMarked(int x, int y) {
        return marked.contains(new Pair<>(x, y));
    };
    
    private void mark(int size,int offset) {
        if (size == 1) {
            marked.add(new Pair<>(offset, offset));
        } else {
            for (int i = 0; i < size; i += size / 2) {
                for (int j = 0; j < size; j += size / 2) {
                    marked.add(new Pair<>(i + offset, j + offset));
                }
            }
            marked.remove(new Pair<>(this.size/2,this.size/2));
        }
    }
    

}
