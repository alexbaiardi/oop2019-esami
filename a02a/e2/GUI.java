package a02a.e2;

import javax.swing.*;

import java.util.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class GUI extends JFrame {
    
    private static final long serialVersionUID = -6218820567019985015L;
    private final Logic logic;
    private final Map<JButton, Pair<Integer, Integer>> buttons;
    
    public GUI(int size) {
        logic=new LogicImpl(size);
        buttons=new HashMap<>();
        
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(size*100, size*100);
        JPanel panel = new JPanel(new GridLayout(size,size));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        JButton south=new JButton(">");
        south.addActionListener(e->{
            logic.update();
            draw();
        });
        this.getContentPane().add(BorderLayout.SOUTH,south);
        
        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();
            if(logic.isMarked(buttons.get(bt).getX(), buttons.get(bt).getY())) {
                System.exit(0);
            }
        };
        for (int i=0;i<size;i++){
            for(int j=0;j<size;j++) {
                final JButton jb = new JButton(" ");
                jb.addActionListener(al);
                panel.add(jb);
                buttons.put(jb, new Pair<>(i, j));
            }
        } 
        draw();
        this.setVisible(true);
    }

    private void draw() {
        buttons.forEach((k,v)->{
            String text=logic.isMarked(v.getX(), v.getY())?"X":" ";
            k.setText(text);            
        });
    }
    
    
}
