package a02a.e2;

public interface Logic {
    
    void update();
    boolean isMarked(int x, int y);

}
