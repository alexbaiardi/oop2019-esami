package a02a.e1;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class WorkflowsFactoryImpl implements WorkflowsFactory {

    private <T> Workflow<T> createFromListofSet(List<Set<T>> tasks){
        
        return new Workflow<T>() {
            
            private final LinkedList<Set<T>> toDo= new LinkedList<>(tasks);

            @Override
            public Set<T> getTasks() {
                return tasks.stream().flatMap(s->s.stream()).collect(Collectors.toSet());
            }

            @Override
            public Set<T> getNextTasksToDo() {
                return isCompleted()?Set.of():toDo.getFirst();
            }

            @Override
            public void doTask(T t) {
                if(!getTasks().contains(t)) {
                    throw new IllegalArgumentException();
                }
                if(!getNextTasksToDo().contains(t)) {
                    throw new IllegalStateException();
                }
                toDo.getFirst().remove(t);
                if(toDo.getFirst().isEmpty()) {
                    toDo.remove();
                }
            }

            @Override
            public boolean isCompleted() {
                return toDo.isEmpty();
            }
            
        };
        
    }
    
    @Override
    public <T> Workflow<T> singleTask(T task) {
        return createFromListofSet(List.of(new HashSet<>(Set.of(task))));
    }

    @Override
    public <T> Workflow<T> tasksSequence(List<T> tasks) {
        
        return createFromListofSet(tasks.stream().map(t->new HashSet<T>(Set.of(t))).collect(Collectors.toList()));
    }

    @Override
    public <T> Workflow<T> tasksJoin(Set<T> initialTasks, T finalTask) {
        return createFromListofSet(List.of(new HashSet<>(initialTasks),new HashSet<>(Set.of(finalTask))));
    }

    @Override
    public <T> Workflow<T> tasksFork(T initialTask, Set<T> finalTasks) {
        return createFromListofSet(List.of(new HashSet<>(Set.of(initialTask)),new HashSet<>(finalTasks)));
    }

    @Override
    public <T> Workflow<T> concat(Workflow<T> first, Workflow<T> second) {
        return createFromListofSet(List.of(first.getTasks(),second.getTasks()));
    }

}
