package a02b.e2;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class LogicImpl implements a02b.e2.Logic {

	private final Set<Pair<Integer, Integer>> selected=new HashSet<>();
	private final int size;
	
	
	
	public LogicImpl(int size) {
		this.size = size;
	}



	@Override
	public void hit(int x, int y) {
		selected.add(new Pair<>(x, y));
	}



	@Override
	public void update() {
		for (Pair<Integer,Integer> p: List.copyOf(selected)) {
			selected.add(new Pair<>(((p.getX()+1+(size-2))%((size-1)*2))-(size-2), p.getY()));
			selected.remove(p);
		}
	}


	@Override
	public boolean getValue(int x, int y) {
		return selected.contains(new Pair<>(x, y)) || selected.contains(new Pair<>(-x, y)) ;
	}

}
