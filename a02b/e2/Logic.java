package a02b.e2;

public interface Logic {
	
	void hit(int x, int y);
	
	void update();
	
	boolean getValue(int x,int y);

}
