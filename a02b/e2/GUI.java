package a02b.e2;

import javax.swing.*;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.awt.*;
import java.awt.event.ActionListener;

public class GUI extends JFrame {
    
	private static final long serialVersionUID = 1L;
	private final Logic logic;
	private final Map<JButton, Pair<Integer, Integer>> buttons;
	
	public GUI(int size) {
		logic=new LogicImpl(size);
		buttons= new HashMap<>();
    	this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(size*100, size*100);
        JPanel panel = new JPanel(new GridLayout(size,size));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        final JButton btMove = new JButton(">");
        ActionListener move = (e)->{
            logic.update();
            draw();
        };
        btMove.addActionListener(move);
        this.getContentPane().add(BorderLayout.SOUTH,btMove);
        
        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();
            logic.hit(buttons.get(bt).getX(), buttons.get(bt).getY());
            draw();
            //bt.setText("X");
        };
        for (int i=0;i<size;i++){
        	for(int j=0;j<size;j++) {
        		final JButton jb = new JButton(" ");
        		jb.addActionListener(al);
        		panel.add(jb);
        		buttons.put(jb,new Pair<>(i, j));
        	}
        } 
        this.setVisible(true);
    }

	private void draw() {
		buttons.forEach((b,c)->{
			String text = logic.getValue(c.getX(), c.getY())?"X":" ";
			b.setText(text);
		});

	}
    
}
