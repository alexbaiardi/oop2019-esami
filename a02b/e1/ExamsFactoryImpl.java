package a02b.e1;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ExamsFactoryImpl implements ExamsFactory {

	
	@Override
	public CourseExam<SimpleExamActivities> simpleExam() {
		List<Pair<List<SimpleExamActivities>, Integer>> a=List.of(new Pair<>(new LinkedList<>(List.of(SimpleExamActivities.WRITTEN,SimpleExamActivities.ORAL, SimpleExamActivities.REGISTER)),1));
		return new CourseExamImpl<>(a);
	}

	@Override
	public CourseExam<OOPExamActivities> simpleOopExam() {
		List<Pair<List<OOPExamActivities>, Integer>> ac=List.of(new Pair<>(new LinkedList<>(List.of(OOPExamActivities.LAB_REGISTER,OOPExamActivities.LAB_EXAM)),1),
														new Pair<>(new LinkedList<>(List.of(OOPExamActivities.PROJ_PROPOSE,OOPExamActivities.PROJ_SUBMIT,OOPExamActivities.PROJ_EXAM)),1),
														new Pair<>(new LinkedList<>(List.of(OOPExamActivities.FINAL_EVALUATION)),2));		
		return new CourseExamImpl<ExamsFactory.OOPExamActivities>(ac);
	}

	@Override
	public CourseExam<OOPExamActivities> fullOopExam() {
		List<Pair<List<OOPExamActivities>, Integer>> ac=List.of(new Pair<>(new LinkedList<>(List.of(OOPExamActivities.STUDY)),1),
				new Pair<>(new LinkedList<>(List.of(OOPExamActivities.LAB_REGISTER,OOPExamActivities.LAB_EXAM)),2),
				new Pair<>(new LinkedList<>(List.of(OOPExamActivities.PROJ_PROPOSE,OOPExamActivities.PROJ_SUBMIT)),2),
				new Pair<>(new LinkedList<>(List.of(OOPExamActivities.CSHARP_TASK)),3),
				new Pair<>(new LinkedList<>(List.of(OOPExamActivities.PROJ_EXAM)),3),
				new Pair<>(new LinkedList<>(List.of(OOPExamActivities.FINAL_EVALUATION)),4));		
return new CourseExamImpl<ExamsFactory.OOPExamActivities>(ac);
	}

}
