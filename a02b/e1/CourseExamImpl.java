package a02b.e1;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


public class CourseExamImpl<X> implements CourseExam<X> {
	List<Pair<List<X>, Integer>> activities;
	private int currentAct;
	
	
	public CourseExamImpl(List<Pair<List<X>, Integer>> activities) {
		this.activities = new LinkedList<>(activities);
		this.currentAct = 1;
	}

	@Override
	public Set<X> getPendingActivities() {
		return activities.stream().filter(e->e.getY()==currentAct).map(e->e.getX()).filter(l->!l.isEmpty()).map(l->l.get(0)).collect(Collectors.toSet());
	}
	
	@Override
	public boolean examOver() {
		return activities.isEmpty();
	}
	
	@Override
	public void completed(X a) {
		activities.stream().filter(e->e.getY()==currentAct).map(e->e.getX()).filter(l->l.contains(a)).findFirst().get().remove(a);
		activities.removeIf(p->p.getX().isEmpty());
		if(activities.stream().filter(p->p.getY()==currentAct).findAny().isEmpty()) {
			currentAct++;
		}
	}
	
	@Override
	public Set<X> activities() {
		return activities.stream().flatMap(e->e.getX().stream()).collect(Collectors.toSet());
	}

}
