package a01b.e2;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class LogicImpl implements Logic {

	private final Set<Pair<Integer, Integer>> mines;
	private final int size;
	private int attempts;
	
	
	public LogicImpl(int size,int numMines) {
		this.size = size;
		attempts=size*size-numMines;
		mines=new HashSet<>();
		createMines(numMines);
		
	}

	private void createMines(int numMines) {
		Random r=new Random();
		Pair<Integer,Integer> mine=new Pair<>(r.nextInt(size), r.nextInt(size));
		mines.add(mine);
		System.out.println("A="+mine.getX()+" B="+mine.getY());
		do {
			mine=new Pair<>(r.nextInt(size), r.nextInt(size));
		}while(!mines.add(mine));
		System.out.println("A="+mine.getX()+" B="+mine.getY());
	}

	@Override
	public boolean hit(int x, int y) {
		if(!mines.contains(new Pair<>(x, y))&&attempts!=1){
			attempts--;
			return false;
		}
		return true;
	}

	@Override
	public int adjacent(int x, int y) {
		int cont=0;
		for(int i=Math.max(0, (x-1));i<=Math.min(size-1,(x+1));i++) {
			for(int j=Math.max(0, (y-1));j<=Math.min(size-1, (y+1));j++) {
				if(mines.contains(new Pair<>(i, j))){
					cont++;
				}
			}
		}
		return cont;
	}

	@Override
	public boolean getRes() {
		return attempts==1;
	}

}
