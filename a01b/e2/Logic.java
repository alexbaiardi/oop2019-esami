package a01b.e2;

public interface Logic {
	
	boolean hit(int x,int y);
	int adjacent(int x,int y);
	boolean getRes();
}
