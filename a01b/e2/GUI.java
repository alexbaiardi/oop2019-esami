package a01b.e2;

import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class GUI extends JFrame {

	private final Map<JButton, Pair<Integer, Integer>> buttons;
	private final Logic logic;
     
    public GUI(int size, int mines) {
    	buttons=new HashMap<>();
    	logic=new LogicImpl(size,mines);
    	this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(size*100, size*100);
        JPanel panel = new JPanel(new GridLayout(size,size));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();
            Pair<Integer,Integer> coord=new Pair<>(buttons.get(bt).getX(), buttons.get(bt).getY());
            if(logic.hit(coord.getX(),coord.getY())) {
            	System.out.println(logic.getRes()?"Hai vinto":"Hai perso");
            	System.exit(0);
            };
            bt.setEnabled(false);
            bt.setText(String.valueOf(logic.adjacent(coord.getX(), coord.getY())));
        };
        for (int i=0;i<size;i++){
        	for(int j=0;j<size;j++) {
	            final JButton jb = new JButton(" ");
	            jb.addActionListener(al);
	            panel.add(jb);
	            buttons.put(jb,new Pair<>(i, j));
        	}
        } 
        this.setVisible(true);
    }
    
}
