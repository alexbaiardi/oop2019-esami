package a01b.e1;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class GridFactoryImpl implements GridFactory {

	@Override
	public <E> Grid<E> create(int rows, int cols) {
		return new Grid<E> (){
			
			List<Cell<E>> grid=Stream.iterate(new Cell<E>(0,0,null), c->c.getRow()<rows, e->new Cell<E>(e.getRow()+1, 0, null))
					.flatMap(c->Stream.iterate(new Cell<E>(c.getRow(),0,null), e->e.getColumn()<cols, p->new Cell<E>(p.getRow(),p.getColumn()+1,null)))
					.collect(Collectors.toList());

			@Override
			public int getRows() {
				return rows;
			}

			@Override
			public int getColumns() {
				return cols;
			}

			@Override
			public E getValue(int row, int column) {
				return grid.stream().filter(c->c.getColumn()==column&&c.getRow()==row).findFirst().get().getValue();
			}

			@Override
			public void setColumn(int column, E value) {
				grid.removeIf(c->c.getColumn()==column);
				grid.addAll(Stream
						.iterate(new Cell<E>(0,column,value), c->c.getRow()<rows, e->new Cell<E>(e.getRow()+1, column, e.getValue()))
						.collect(Collectors.toList()));
			}

			@Override
			public void setRow(int row, E value) {
				grid.removeIf(c->c.getRow()==row);
				grid.addAll(Stream
						.iterate(new Cell<E>(row,0,value), c->c.getColumn()<cols, e->new Cell<E>(row, e.getColumn()+1, e.getValue()))
						.collect(Collectors.toList()));
				
			}

			@Override
			public void setBorder(E value) {
				setRow(0, value);
				setRow(rows-1, value);
				setColumn(0, value);
				setColumn(cols-1, value);
			}

			@Override
			public void setDiagonal(E value) {
				grid.removeIf(c->c.getRow()-c.getColumn()==0);
				grid.addAll(Stream
						.iterate(new Cell<E>(0,0,value), c->c.getRow()<Math.min(rows, cols), e->new Cell<E>(e.getRow()+1, e.getColumn()+1, e.getValue()))
						.collect(Collectors.toList()));
			}

			@Override
			public Iterator<Cell<E>> iterator(boolean onlyNonNull) {
				return grid.stream()
						.filter(c->!onlyNonNull||c.getValue()!=null)
						.sorted((c1,c2)->c1.getRow()==c2.getRow()?c1.getColumn()-c2.getColumn():c1.getRow()-c2.getRow())
						.iterator();
			}
		};
		
	}

}
